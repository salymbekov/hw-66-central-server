<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="app_index")
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportAndEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/check-client/{password}/{email}", name="check_client_exists")
     * @Method("HEAD")
     * @param string $password
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function CheckClientExistsAction(
        string $password,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($password, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/return-client/{email}", name="return-client")
     * @Method("GET")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientReturnAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $client = $clientRepository->findOneByPassportAndEmail('', $email);
        $data = [
            'email'=> $client->getEmail(),
            'password'=> $client->getPassword(),
            'passport'=>$client->getPassport()
        ];
        return new JsonResponse($data);
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');

        if(empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }

        if ($clientRepository->findOneByPassportAndEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'],406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }
}
