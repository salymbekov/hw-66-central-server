<?php
namespace App\Command;

use App\Model\User\UserHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RegisterOrganizationCommand extends Command implements ContainerAwareInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(?string $name = null, EntityManagerInterface $entityManager, UserHandler $userHandler)
    {
        parent::__construct($name);

        $this->entityManager = $entityManager;
        $this->userHandler = $userHandler;
    }

    protected function configure()
    {
        $this
            ->setName('api:organization:register')
            ->addOption(
                'organizationName',
                'o',
                InputOption::VALUE_REQUIRED,
                'Name of organizations'
            )
            ->addOption(
                'organizationSiteURL',
                'u',
                InputOption::VALUE_REQUIRED,
                'The address of the site of the organization on which this API will be used'
            )
            // the short description shown while running "php bin/console list"
            ->setDescription('Registration of a new organization')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to register a new organization and get an API token for it')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $organizationName = $input->getOption('organizationName');
        $organizationSiteURL = $input->getOption('organizationSiteURL');

        $user = $this->userHandler->createNew($organizationName, $organizationSiteURL);

        $output->writeln([
            'New organization',
            '================',
            'id: '.$user->getId(),
            'organizationName: '.$user->getOrganizationName(),
            'organizationSiteURL: '.$user->getOrganizationSiteURL(),
            'token: '.$user->getToken(),
            '=================================================================',
            'Write down this data somewhere in a safe place, so as not to lose'
        ]);
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
